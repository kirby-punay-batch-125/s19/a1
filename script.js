let mew = new Pokemon("mew", 10, 50);
let ditto = new Pokemon("ditto", 10, 45);

function Pokemon(name, lvl, hp){
  this.name = name;
  this.level = lvl;
  this.health = hp * 2;
  this.attack = lvl;
  this.tackle = function(target){
    console.log(target)
    console.log(`${this.name} tackled ${target.name}`);
    target.health -= this.attack;
    console.log(`${target.name}'s is now reduced to ${target.health}`)
    if(target.health <= 10){
        faint(target.name);
    }
  };
  faint = function(name){
    console.log(`${this.name} fainted`);
  };
}

console.log(mew.tackle(ditto))
console.log(mew.tackle(ditto))
console.log(mew.tackle(ditto))
console.log(mew.tackle(ditto))
console.log(mew.tackle(ditto))
console.log(mew.tackle(ditto))
console.log(mew.tackle(ditto))
console.log(mew.tackle(ditto))
console.log(mew.tackle(ditto))
